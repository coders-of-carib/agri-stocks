<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CropController;
use App\Models\Crop;
use App\Models\Offering;
use Illuminate\Support\Facades\Auth;

class OfferingController extends Controller
{

    //populates page for making orders
    public function index(Request $request){

        $crops = Crop::all();

        return view('Pages.farmer', compact('crops'));

    }

    //function to make an offering.
    public function makeOrder(Request $request){

        $offer = new Offering();

        $user = Auth::user();

        // $offer->id = $user->id;
        $offer->first_name = 'James';
        $offer->last_name = 'Harden';
        $offer->crop = $request->crop;
        $offer->price  = $request->price;
        $offer->quantity = $request->quantity;
        $offer->amount_paid=0;
        $offer->farmer_id=1;
        $offer->status = 'Active';

        $offer->save();

        return back();

    }

    //publish offerings
    //investir
    public function publishOffer(){

        $offerings = Offering::all();
        return view('myView', compact('offerings'));
    }
    
    
     public function offeringsMade(Request $request){

        $user = Auth::user();

         $crops = Crop::all();

      


        $user_id = $user->id;

        $offerings = Offering::where('farmer_id', $user_id)->get();

        return view('Pages.farmer', compact('offerings', 'crops'));
    }
    
        public function offeringsinvest(Request $request){

        $user = Auth::user();

         $crops = Crop::all();

      


        $user_id = $user->id;

        $offerings = Offering::where('farmer_id', $user_id)->get();

        return view('Pages.offers', compact('offerings', 'crops'));
    }
}
