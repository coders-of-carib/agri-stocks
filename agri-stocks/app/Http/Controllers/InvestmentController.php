<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Investment;
use Illuminate\Support\Facades\Auth;
use App\Models\Investor;
use DB;

class InvestmentController extends Controller
{
    //
     //view portfolio for invetors
    public function portfolio(){

        $user = Auth::user();

        $user_id = $user->id;

        $investments = Investment::where('investor_id', $user_id)->get();

        return view('Pages.investor', compact('investments'));
    }

    //function to add investment to db from investors.
    public function addInvestment(Request $request){

        $user = Auth::user();

        $user_id = $user->id;

        $amount = $request->amount_investing;
        $offer_id = $request->offer_id;

        DB::table('offerings')
            ->where('id', $offer_id)
            ->update(['amount_paid' =>((int)$amount)]);
            // 'amount_paid' + 

        $investment = new Investment();

        // $request->farmer_id
        // $request->crop_id;
        $investment->farmer_id =1 ;
        $investment->investor_id = $user_id;
        $investment->crop_id = 1;
        $investment->amount = $amount;
        $investment->save();
        
        return back();

    }
}
