<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Pages.index');
});

Route::get('/invest', function(){
    return view('Pages.investor');
});

Auth::routes();

//return all crops with data to offering page for selection by farmer.
//farmer uses this data to create his offering. he would say select his ptoducts from a list or group
Route::get('/offering', 'OfferingController@index')->name('offering');

//adds offer to db.
Route::post('/add/offering', 'OfferingController@makeOrder')->name('add.offer');

//investors view offers posted
//this a fi di investor them see every single posting from every farmer 
Route::get('/view/offerings', 'OfferingController@publishOffer')->name('view.offerings');

//Farmers to view their offerings
//fi farmer see all the offers weh them did mek 
Route::get('/view/offers', 'OfferingController@offeringsMade');

Route::get('/page', function(){
    return view('Pages.page');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/offers', 'OfferingController@offeringsinvest');
//route to show portfolio for investors
Route::get('/my/portfolio', 'InvestmentController@portfolio');

Route::post('/add/offering', 'InvestmentController@addInvestment');
