@extends('layout.dash')

@section('dash')

@if(count($investments)>0)

<div class="row">
    <div class="card-deck">
    @foreach($investments as$investment)
    
        <div class="col-sm-3">
  <div class="card mb-4">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="{{asset('img/card.jpg')}}" alt="Card image cap">
       <a href="#!">
          <div class="mask rgba-white-light"></div>
        </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
    <h4 class="card-title">{{$investment[$loop->index]['crop_id']}}</h4>
      <!--Text-->
        
      <ul>
          <li>
            <strong>Farmer:</strong> James Harden
          </li>
          <li>
              <strong>Investors:</strong> Phillip Newman
          </li>
          <li>
              <strong>Amount Invested:</strong> {{$investment->amount}}
               <div class="progress progress-sm mb-3">
                 <div id="progress-bar-example-1" class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
          </li>
          <li>
              <strong>Created at:</strong> {{$investment->created_at}}
          </li>
      </ul>
      <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
    <a href="" class="btn btn-danger btn-md" data-toggle="modal" data-target="#modalLoginForm">Canel</a>
    </div>

  </div>
  <!-- Card -->
  </div>
        @endforeach
        {{-- //Displays pagination links --}}
    @else 
            <h3>No Posts Found!</h3>

 @endif

@endsection