@extends('layout.dash')

@section('dash')

{{-- <div class="container"> --}}
 @if(count($offerings)>0)


<div class="row">
    <div class="card-deck">
    @foreach($offerings as $offering)


    <div class="col-sm-3">
  <div class="card mb-4">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="{{asset('img/card.jpg')}}" alt="Card image cap">
       <a href="#!">
          <div class="mask rgba-white-light"></div>
        </a>
    </div>

    <!--Card content-->
    <div class="card-body">

      <!--Title-->
    <h4 class="card-title">{{$crops[$loop->index]['name']}}</h4>
      <!--Text-->
        
      <ul>
          <li>
            <strong> Farmer: </strong> {{$offering->first_name}} {{$offering->last_name}}
          </li>
          <li>
              <strong>Quantity:</strong> {{$offering->quantity}}
          </li>
          <li>
              <strong>Amount Required:</strong> {{$offering->price}}
              <div class="progress progress-sm mb-3">
                 <div id="progress-bar-example-1" class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
          </li>
          <li>
              <strong>Status:</strong> <span class="badge badge-success">{{$offering->status}}</span>
          </li>
      </ul>
      <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
    <a href="" class="btn btn-light-blue btn-md" data-toggle="modal" data-target="#modalLoginForm">Invest</a>
    </div>

  </div>
  <!-- Card -->
  </div>
        @endforeach
        {{-- //Displays pagination links --}}
    @else 
            <h3>No Posts Found!</h3>

 @endif
    {{-- </div> --}}

   <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Invest</h4>
      <a href="{{url('/offering')}}">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </a>
      </div>
      <div class="modal-body mx-3">

        <form action="{{url('/add/offering')}}" method="post">
            @csrf
      
        <div class="md-form mb-4">
         
          <input type="number" placeholder="Investment Amount" name="amount_investing" class="form-control validate">
          <label data-error="wrong" data-success="right"  ></label>
        </div>
       
        @isset($crops)
        <div class="md-form mb-4">
            <select class="browser-default custom-select" name="crop">
                     @foreach($crops as $crop)
            <option value="{{$crop->id}}" name="offer_id">{{$crop->name}}</option>
                    @endforeach
            </select>             
               
        </div>
        @endisset

        {{-- <div class="md-form mb-4">
          <i class="fas fa-lock prefix grey-text"></i>
          <input type="text" id="price" placeholder="Investment amount" name="price" class="form-control validate">
          <label data-error="wrong" data-success="right"></label>
        </div> --}}
   
      </div>

       

      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default" type="submit">Submit</button>
        
      </div>
       </form>
    </div>
  </div>
</div>



@endsection