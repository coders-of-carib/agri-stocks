<!doctype html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Gro-Fund</title>
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="{{asset('Dashboard/HTML/styles/shards-dashboards.1.1.0.min.css')}}">
    <link rel="stylesheet" href="{{asset('Dashboard/HTML/styles/extras.1.1.0.min.css')}}">
    <script async defer src="https://buttons.github.io/buttons.js"></script>
         <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <link rel="stylesheet" href="{{asset('MDBootstrap/css/mdb.min.css')}}">
    <script src="{{asset('MDBootstrap/js/mdb.min.js')}}}"></script> 
    {{-- <style>
        .back{background-image: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 75%);">
        }
        </style> --}}
  </head>
  <body class="h-100 back aqua-gradient color-block mb-3 mx-auto z-depth-1-half" style="overflow:hidden" >

 
    @include('inc.navbar')
   
    @yield('content')
  </body>
</html>